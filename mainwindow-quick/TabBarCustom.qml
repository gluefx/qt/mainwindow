import QtQuick 2.14
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.14

Item {
    id: root
    width: parent.width
    height: 100

    ListModel {
        id: tabModel
        ListElement { title: "First" }
        ListElement { title: "Second" }
        ListElement { title: "Third" }
        ListElement { title: "Fourth" }
        ListElement { title: "Fifth" }
    }

    Component {
        id: tabDelegate

        DropArea {
//            width: Math.max(100, bar.width / 5)
            width: 100
            height: 40

            onEntered: {
                console.log('drop enter', drag.source.buttonIndex, index);
                tabModel.move(index, drag.source.buttonIndex, 1);
            }

            Button {
                id: tabButton
//                anchors.fill: parent
//                width: Math.max(100, bar.width / 5)
//                anchors.fill: parent
                property int buttonIndex: index
                // GX NOTE: https://doc.qt.io/qt-5/qtquickcontrols2-customize.html#customizing-button
                contentItem: Text {
                    text: title
                    //font: control.font
                    //opacity: enabled ? 1.0 : 0.3
                    color: index == bar.currentIndex ? "#353637" : "#fff"
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    //elide: Text.ElideRight
                }
                background: Rectangle {
                    implicitWidth: 100
                    implicitHeight: 40
                    //opacity: enabled ? 1 : 0.3
                    //border.color: index === bar.currentIndex ? "#17a81a" : "#21be2b"
                    //border.width: 1
                    //radius: 2
                    color: index == bar.currentIndex ? "#fff" : "#353637"
                }

                DragHandler {
                    id: dragHandler
                }

                Drag.active: dragHandler.active
                Drag.source: tabButton
//                Drag.hotSpot.x: 36
//                Drag.hotSpot.y: 36

                onClicked: {
                    bar.currentIndex = index;
                }

//                MouseArea {
//                    anchors.fill: parent
//                    onClicked: {
//                        console.log('MouseArea click', index)
//                        bar.currentIndex = index;
//                        //tabModel.move(2, 3, 1);
//                    }
////                    onMouseXChanged: {
////                        console.log('onMouseXChanged', index)
////                    }
//                }
            }
        }


    }

    ListView {
        id: bar
        property var currentIndex: 0
        orientation: Qt.Horizontal
        anchors.fill: parent
        width: parent.width
        model: tabModel
        delegate: tabDelegate
        //highlight: Rectangle { color: "lightsteelblue"; radius: 5 }
        focus: true
        displaced: Transition {
            NumberAnimation { properties: "x,y"; easing.type: Easing.OutQuad }
        }
    }

    StackLayout {
        anchors.top: bar.bottom
        width: parent.width
        currentIndex: bar.currentIndex
        Repeater {
            model: tabModel

            Item {
                Text {
                    text: title
                }
            }
        }
    }
}
