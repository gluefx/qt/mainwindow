import QtQuick 2.0
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.14

Item {
    id: root
    width: parent.width
//    width: 400
    height: 100
    property var tabModel: ["First", "Second", "Third", "Fourth", "Fifth"]

    TabBar {
        id: bar
        width: parent.width
        Repeater {
            model: root.tabModel

            TabButton {
                text: modelData
                width: Math.max(100, bar.width / 5)
                onClicked: {
                    console.log('click')
                }

                // GX NOTE: if added "MouseArea", TabButton cannot change

//                MouseArea {
//                    anchors.fill: parent
////                    onClicked: {
////                        console.log('click')
////                        parent.clicked();
////                    }
//                }
            }
        }
    }

    StackLayout {
        anchors.top: bar.bottom
        width: parent.width
        currentIndex: bar.currentIndex
        Repeater {
            model: root.tabModel

            Item {
              Text {
                  text: modelData
              }
            }
        }
    }
}
