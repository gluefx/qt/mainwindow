/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.0
import QtQml.Models 2.1
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.14

Rectangle {
    id: root

//    width: 600; height: 400
    anchors.fill: parent
//    width: parent.width

    Component {
        id: dragDelegate

        MouseArea {
            id: dragArea
            property bool held: false

            //GX NOTE: doesn't work, error: Invalid alias reference. An alias reference must be specified as <id>, <id>.<property> or <id>.<value property>.<property>
            //property alias currentIndex: dragArea.DelegateModel.model.currentIndex

            //anchors { left: parent.left; right: parent.right }
            width: 100
            height: content.height

            drag.target: held ? content : undefined
            //drag.axis: Drag.YAxis

            // GX TODO: Hold time is too long
            onPressAndHold: held = true
            onReleased: held = false
            onClicked: {
                dragArea.DelegateModel.model.currentIndex = index;
            }

            Rectangle {
                id: content
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    verticalCenter: parent.verticalCenter
                }
                width: dragArea.width;
                height: column.implicitHeight + 4

                border.width: 1
                border.color: dragArea.DelegateModel.model.currentIndex === index ? "gray" : "lightsteelblue"

                color: dragArea.held ? "lightsteelblue" : "white"
                Behavior on color { ColorAnimation { duration: 100 } }

                radius: 2

                Drag.active: dragArea.held
                Drag.source: dragArea
                Drag.hotSpot.x: width / 2
                Drag.hotSpot.y: height / 2

                states: State {
                    when: dragArea.held

                    ParentChange { target: content; parent: root }
                    AnchorChanges {
                        target: content
                        anchors { horizontalCenter: undefined; verticalCenter: undefined }
                    }
                }

                Row {
                    id: column
                    anchors { fill: parent; margins: 2 }

                    Text { text: name }
                    Text { text: ' x' }
                }
            }

            DropArea {
                anchors { fill: parent; margins: 10 }

                // GX TODO: modify tabs in onEntered leaves user no choise.
                // should add placeholder in onEntered and do the real modification in onReleased
                onEntered: {
                    var dragModel = drag.source.DelegateModel;
                    var dropModel = dragArea.DelegateModel

                    var dragModelName = dragModel.model.name;
                    var dropModelName = dropModel.model.name;

                    var dragDataIndex = dragModel.itemsIndex;
                    var dropDataIndex = dropModel.itemsIndex;

                    if (dragModelName === dropModelName) {
                        if (dragDataIndex === dropDataIndex) {
                            return;
                        }
                        dropModel.model.items.move(dragDataIndex, dropDataIndex);
                    } else {
                        var dragDataName = dragModel.model.items.get(dragDataIndex).model.name;
                        dragModel.model.items.remove(dragDataIndex);
                        dropModel.model.items.insert(dropDataIndex, { name: dragDataName });
                    }
                }
            }
        }
    }

    ListModel {
        id: listModel
        ListElement {
            name: "One";
//            age: 1;
        }
        ListElement {
            name: "Two";
//            age: 2;
        }
        ListElement {
            name: "Three";
//            age: 3;
        }
    }

    DelegateModel {
        id: visualModel

        model: listModel
        delegate: dragDelegate

        // GX NOTE: add modelName and currentIndex here, decouple delegate component and specific model
        property var name: 'visualModel'
        property var currentIndex: 1
    }

    DelegateModel {
        id: visualModel2

        model: listModel
        delegate: dragDelegate

        property var name: 'visualModel2'
        property var currentIndex: 2
    }

    ListView {
        id: bar

        property var currentIndex: 0

//        anchors { fill: parent; margins: 2 }
        width: parent.width
        height: 60
        anchors.top: parent.top

        model: visualModel

        spacing: 4
        cacheBuffer: 50
        orientation: Qt.Horizontal
    }

    StackLayout {
        id: content
        anchors.top: bar.bottom
        width: parent.width
        height: 60
        currentIndex: visualModel.currentIndex
        Repeater {
            model: visualModel.model

            Item {
                Text {
                    text: name
                }
            }
        }
    }

    Text {
        id: separator
        anchors.top: content.bottom
        width: 100
        height: 40
        text: "**** second ****"
    }

    ListView {
        id: bar2

        property var currentIndex: 0

//        anchors { fill: parent; margins: 2 }
        width: parent.width
        height: 60
        anchors.top: separator.bottom

        model: visualModel2

        spacing: 4
        cacheBuffer: 50
        orientation: Qt.Horizontal
    }

    StackLayout {
        id: content2
        anchors.top: bar2.bottom
        width: parent.width
        height: 60
        currentIndex: visualModel2.currentIndex
        Repeater {
            model: visualModel2.model

            Item {
                Text {
                    text: name
                }
            }
        }
    }
}
