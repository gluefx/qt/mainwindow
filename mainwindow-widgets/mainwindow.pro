TEMPLATE = app
QT += widgets
requires(qtConfig(combobox))

HEADERS += colorswatch.h mainwindow.h toolbar.h \
    qdesktopwidget.h \
    qdesktopwidget_p.h \
    qdockarealayout_p.h \
    qdockwidget.h \
    qdockwidget_p.h \
    qmainwindow.h \
    qmainwindowlayout_p.h \
    qstylesheetstyle_p.h \
    qtoolbar.h \
    qtoolbar_p.h \
    qtoolbararealayout_p.h \
    qtoolbarextension_p.h \
    qtoolbarlayout_p.h \
    qtoolbarseparator_p.h \
#    qwidget.h \
#    qwidget_p.h \
    qwidgetanimator_p.h
SOURCES += colorswatch.cpp mainwindow.cpp toolbar.cpp main.cpp \
    qdesktopwidget.cpp \
    qdockarealayout.cpp \
    qdockwidget.cpp \
    qmainwindow.cpp \
    qmainwindowlayout.cpp \
    qstylesheetstyle.cpp \
    qstylesheetstyle_default.cpp \
    qtoolbar.cpp \
    qtoolbararealayout.cpp \
    qtoolbarextension.cpp \
    qtoolbarlayout.cpp \
    qtoolbarseparator.cpp \
#    qwidget.cpp \
    qwidgetanimator.cpp
build_all:!build_pass {
    CONFIG -= build_all
    CONFIG += release
}

RESOURCES += mainwindow.qrc

# install
# target.path = $$[QT_INSTALL_EXAMPLES]/widgets/mainwindows/mainwindow
# INSTALLS += target

INCLUDEPATH += /Users/guanxu/Qt/5.14.0/clang_64/lib/QtWidgets.framework/Versions/5/Headers/5.14.0
INCLUDEPATH += /Users/guanxu/Qt/5.14.0/clang_64/lib/QtWidgets.framework/Versions/5/Headers/5.14.0/QtWidgets
INCLUDEPATH += /Users/guanxu/Qt/5.14.0/clang_64/lib/QtWidgets.framework/Versions/5/Headers/5.14.0/QtWidgets/private
INCLUDEPATH += /Users/guanxu/Qt/5.14.0/clang_64/lib/QtGui.framework/Versions/5/Headers/5.14.0
INCLUDEPATH += /Users/guanxu/Qt/5.14.0/clang_64/lib/QtGui.framework/Versions/5/Headers/5.14.0/QtGui
INCLUDEPATH += /Users/guanxu/Qt/5.14.0/clang_64/lib/QtCore.framework/Versions/5/Headers/5.14.0
INCLUDEPATH += /Users/guanxu/Qt/5.14.0/clang_64/lib/QtCore.framework/Versions/5/Headers/5.14.0/QtCore
INCLUDEPATH += /Users/guanxu/Qt/5.14.0/clang_64/include
